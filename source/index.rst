.. Titanic Formation documentation master file, created by
   sphinx-quickstart on Fri Sep 17 09:35:25 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Titanic Formation's documentation!
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: src.feature_engineering
   :members:
   :undoc-members:
   :show-inheritance:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
